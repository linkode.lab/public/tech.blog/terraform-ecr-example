# ---
# CodePipeline を起動させる Event Bridge

# Event Bridge の Role
data "aws_iam_policy_document" "eventbridge" {
  statement {
    effect    = "Allow"
    resources = ["${aws_codepipeline.example.arn}"]

    actions = [
      "codepipeline:StartPipelineExecution",
    ]
  }
}

module "eventbridge_role" {
  source     = "./iam_role"
  name       = "tf-example_eventbridge"
  identifier = "events.amazonaws.com"
  policy     = data.aws_iam_policy_document.eventbridge.json
}

# Event Bridge 本体
resource "aws_cloudwatch_event_rule" "ecr_push" {
  name        = "ecr_push"
  description = "ECR にイメージを Push されたら、 CodePipeline のイベントを発火する"

  event_pattern = jsonencode(
    {
      "source" : [
        "aws.ecr"
      ],
      "detail-type" : [
        "ECR Image Action"
      ],
      "detail" : {
        "action-type" : [
          "PUSH"
        ],
        "result" : [
          "SUCCESS"
        ],
        "repository-name" : [
          "example-repo"
        ]
      }
    }
  )
}

# イベント発生時に実行する処理のターゲット
resource "aws_cloudwatch_event_target" "ecr" {
  rule      = aws_cloudwatch_event_rule.ecr_push.name
  target_id = "CodePipeline"
  arn       = aws_codepipeline.example.arn
  role_arn  = module.eventbridge_role.iam_role_arn
}