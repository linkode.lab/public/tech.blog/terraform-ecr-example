variable "certificate_arn" {
    type = string
    default = "arn:aws:acm:ap-northeast-1:740204728684:certificate/5c8dfd95-7239-413e-ad52-e026451cf75b"
}

variable "codepipeline_config_s3bucket" {
    type = string
    default = "tf-example"
}

variable "app_port" {
    type = number
    default = 3000
}