resource "aws_sqs_queue" "example" {
  name                      = "example"
  max_message_size          = 2048
  message_retention_seconds = 60 * 60 * 24 * 1
  policy                    = data.aws_iam_policy_document.example-receive.json
}

data "aws_iam_policy_document" "example-receive" {
  statement {
    effect = "Allow"
    actions = [
      "sqs:SendMessage",
    ]
  }
}

