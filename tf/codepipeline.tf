# CodePipeline で使用する IAM ロール
data "aws_iam_policy_document" "codepipeline" {
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketVersioning",
      "ecr:DescribeImages",
      "ecs:DescribeServices",
      "ecs:DescribeTaskDefinition",
      "ecs:DescribeTasks",
      "ecs:ListTasks",
      "ecs:RegisterTaskDefinition",
      "ecs:UpdateService",
      "iam:PassRole",
    ]
  }
}

module "codepipeline_role" {
  source     = "./iam_role"
  name       = "tf-example-codepipeline"
  identifier = "codepipeline.amazonaws.com"
  policy     = data.aws_iam_policy_document.codepipeline.json
}

# CodePipeline 本体
resource "aws_codepipeline" "example" {
  name     = "example"
  role_arn = module.codepipeline_role.iam_role_arn

  stage {
    name = "Source"

    action {
      name             = "ECRSetting"
      category         = "Source"
      owner            = "AWS"
      provider         = "ECR"
      version          = 1
      output_artifacts = ["ECROutArtifact"]

      configuration = {
        ImageTag       = "latest",
        RepositoryName = aws_ecr_repository.example.name
      }
    }

    action {
      name             = "S3Setting"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = 1
      output_artifacts = ["S3OutArtifact"]

      configuration = {
        S3Bucket             = var.codepipeline_config_s3bucket,
        S3ObjectKey          = "imagedefinitions.json.zip",
        PollForSourceChanges = "false"
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "ECS"
      version         = 1
      input_artifacts = ["S3OutArtifact"]

      configuration = {
        ClusterName       = aws_ecs_cluster.example.name
        ServiceName       = aws_ecs_service.example.name
        DeploymentTimeout = 10
      }
    }
  }

  artifact_store {
    location = "tf-example"
    type     = "S3"
  }
}