provider "aws" {
  version = "~> 2.0"
  profile = "terraformer"
  region  = "ap-northeast-1"
}

terraform {
  required_version = "0.12.29"
}