import express from 'express';
import endpoint from './endpoints.config';
import * as AWS from "aws-sdk";
import { AWSError } from 'aws-sdk';
AWS.config.update({
    region: endpoint.Region,
});
export const dynamoDB = new AWS.DynamoDB({
    apiVersion: "2012-08-10",
});
export const sqs = new AWS.SQS({ 
    apiVersion: "2012-11-05" 
});
import { PutItemInput, PutItemOutput } from 'aws-sdk/clients/dynamodb';
import { SendMessageRequest, SendMessageResult } from 'aws-sdk/clients/sqs';

const app: express.Express = express()

// CORSの許可
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

// body-parserに基づいた着信リクエストの解析
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Getのルーティング
const router: express.Router = express.Router()
router.get('/', (req:express.Request, res:express.Response) => {
    console.log("Request Received.");
    const id = Math.random().toString(32).substring(2);
    console.log(`id: ${id}`);
    const dbParams: PutItemInput = {
        Item: {
          id: {
            S: id,
          },
          name: {
            S: "example",
          },
        },
        TableName: "Example",
    };
    dynamoDB.putItem(dbParams, function(err: AWSError, data: PutItemOutput) {
        if (err) {
            console.error(err);
          } else {
            console.info(data);
        }
    });

    const sqsParams: SendMessageRequest = {
        QueueUrl: endpoint.QueueUrl,
        MessageBody: `id: ${id}`,
        DelaySeconds: 10,
    };
    sqs.sendMessage(sqsParams, function (err: AWSError, data: SendMessageResult) {
        if (err) {
          console.error(err);
        } else {
          console.info(data);
        }
    });

    res.send(req.query)
})
app.use(router)

// 3000番ポートでAPIサーバ起動
app.listen(endpoint.Port,()=>{ console.log(`Example app listening on port ${endpoint.Port}!`) })
