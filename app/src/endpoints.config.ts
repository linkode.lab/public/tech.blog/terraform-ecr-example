import * as dotenv from 'dotenv';
dotenv.config();

export default {
    Port: process.env.PORT ?? '',
    QueueUrl: process.env.QUEUE_URL ?? '',
    Region: process.env.REGION ?? ''
}